$(document).ready(function () {
    function a(e, t, n) {
        if (n) {
            $slideTo = $(n)
        } else {
            $slideTo = $('.panel[data-slide="' + e + '"]')
        }
        $("html,body").stop().animate({
            scrollTop: $slideTo.offset().top - 60
        }, 2e3, "easeInOutQuint")
    }

    function b(t, n) {
        if (!n) {
            $("#locations-select").val(t)
        }
        if (t == -1) {
            l.fitBounds(h);
            t = 0
        } else {
            var r = new google.maps.LatLng(e[t][1], e[t][2]);
            l.panTo(r);
            l.setZoom(12);
            t++
        } if ($("#locations-info > div:eq(" + t + ")").is(":hidden")) {
            $("#locations-info > div:visible").fadeOut(500, function () {
                $("#locations-info > div:eq(" + t + ")").fadeIn(500)
            })
        }
    }

    function w() {
        var t = {
            zoom: 15,
            scrollwheel: false,
            center: new google.maps.LatLng(e[0][1], e[0][2]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        l = new google.maps.Map(document.getElementById("locations-map"), t);
        var n = new google.maps.InfoWindow,
            r, i;
        for (i = 0; i < e.length; i++) {
            var s = new google.maps.LatLng(e[i][1], e[i][2]);
            c[i] = new google.maps.Marker({
                raiseOnDrag: false,
                icon: p,
                shadow: d,
                shape: v,
                map: l,
                position: s
            });
            h.extend(s);
            google.maps.event.addListener(c[i], "click", function (e, t) {
                return function () {
                    var e = t + 1;
                    n.setContent('<div class="locations-infobox"><h3>' + $("#locations-info > div:eq(" + e + ") > h3").html() + "</h3>" + $("#locations-info > div:eq(" + e + ") > address").html() + "</div>");
                    n.open(l, c[t]);
                    b(t, false)
                }
            }(c[i], i));
            l.fitBounds(h)
        }
    }

    function S() {
        var e = {
            zoom: 15,
            scrollwheel: false,
            center: new google.maps.LatLng(t[1], t[2]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        E = new google.maps.Map(document.getElementById("contact-map"), e)
    }

    function T(e) {
        var t = false;
        var n = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var r = e;
        if (!n.test(r) || r == "") {
            return false
        } else {
            return true
        }
    }

    function N(e, t) {
        if (!t) {
            var t = "error"
        }
        $("#contact-form img.loading").fadeOut(1e3).remove();
        $("#contact-form #response").hide().html('<span class="' + t + '">' + e + "</span>").fadeIn("slow")
    }
    var e = [
        ["Seattle, Washington", 47.55808074240213, -122.30396747589111],
        ["Toronto, Canada", 43.66431708081812, -79.36768054962158],
        ["Miami, Florida", 25.79533186639593, -80.19753456115723]
    ];
    var t = e[0];
    var n = false;
    var r = "Please enter a valid email address",
        i = "Please enter your name",
        s = "Please enter your comment";
    var o = false;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone|ZuneWP7/i.test(navigator.userAgent)) {
        o = true;
        $("html").addClass("mobile")
    }
    if (!o) {
        $(window).stellar();
        var u = false;
        $(".panel").bind("inview", function (e, t, n, r) {
            dataslide = $(this).attr("data-slide");
            if (t) {
                if (r == "both") {
                    $("#top li").not('[data-slide="' + dataslide + '"]').removeClass("active");
                    $('#top li[data-slide="' + dataslide + '"]').addClass("active")
                } else if (r == "top" && u != "both") {
                    $("#top li").not('[data-slide="' + dataslide + '"]').removeClass("active");
                    $('#top li[data-slide="' + dataslide + '"]').addClass("active")
                } else {}
                u = r
            } else {
                $('#top li[data-slide="' + dataslide + '"]').removeClass("active")
            }
        })
    }
    $("#top").find("li, #logo > div").click(function (e) {
        dataslide = $(this).attr("data-slide");
        a(dataslide, true)
    });
    $(".to-top").click(function (e) {
        e.preventDefault();
        a(false, true, $(this).attr("href"))
    });
    $(function () {
        $("a[rel*=external], .social a").click(function () {
            window.open(this.href);
            return false
        })
    });
    $("#top .main_menu").mobileMenu();
    $(".select-menu").customSelect();
    $("#front").flexslider({
        animation: "slide",
        controlNav: false
    });
    if (!o) {
        function f() {
            if ($(window).width() < 1700) {
                $("#grapes, #grape, #wine-glass").fadeOut(function () {
                    $(this).css("z-index", "-500")
                })
            } else {
                $("#grapes, #grape, #wine-glass").fadeIn().css("z-index", "10");
            }
        }
        f();
        $(window).resize(function () {
            f()
        })
    }
    $("#top a").click(function (e) {
        e.preventDefault();
        $("#top li a").removeClass("active");
        $(this).addClass("active")
    });
    $("a[href^='#']").click(function (e) {
        e.preventDefault()
    });
    $(".social li a").hover(function () {
        var e = this;
        $(e).stop().animate({
            marginTop: "-8px",
            paddingBottom: "8px",
            opacity: 1
        }, 250, function () {
            $(e).animate({
                marginTop: "-4px",
                paddingBottom: "4px"
            }, 250)
        })
    }, function () {
        var e = this;
        $(e).stop().animate({
            marginTop: "0px",
            paddingBottom: "0px",
            opacity: .9
        }, 250, function () {
            $(e).animate({
                marginTop: "0px",
                paddingBottom: "0px"
            }, 250)
        })
    });
    $("#gallery .thumbnail").colorbox({
        rel: "gal",
        current: "",
        maxWidth: "85%",
        maxHeight: "85%",
        scrolling: false,
        opacity: .5,
        title: function () {
            return $(this).find("figcaption > div").html()
        }
    });
    var l;
    var c = [];
    var h = new google.maps.LatLngBounds;
    var p = new google.maps.MarkerImage("assets/images/icons/marker.png", new google.maps.Size(38, 55), new google.maps.Point(0, 0), new google.maps.Point(0, 55));
    var d = new google.maps.MarkerImage("assets/images/icons/marker-shadow.png", new google.maps.Size(70, 55), new google.maps.Point(0, 0), new google.maps.Point(0, 55));
    var v = {
        coord: [25, 0, 27, 1, 29, 2, 31, 3, 32, 4, 33, 5, 34, 6, 34, 7, 35, 8, 36, 9, 36, 10, 37, 11, 37, 12, 37, 13, 37, 14, 37, 15, 37, 16, 37, 17, 37, 18, 37, 19, 37, 20, 37, 21, 37, 22, 37, 23, 36, 24, 36, 25, 35, 26, 35, 27, 34, 28, 34, 29, 33, 30, 32, 31, 32, 32, 31, 33, 30, 34, 30, 35, 29, 36, 28, 37, 28, 38, 27, 39, 26, 40, 26, 41, 25, 42, 24, 43, 24, 44, 23, 45, 23, 46, 22, 47, 22, 48, 21, 49, 21, 50, 20, 51, 20, 52, 20, 53, 19, 54, 18, 54, 18, 53, 17, 52, 17, 51, 16, 50, 16, 49, 16, 48, 15, 47, 15, 46, 14, 45, 13, 44, 13, 43, 12, 42, 12, 41, 11, 40, 10, 39, 10, 38, 9, 37, 8, 36, 8, 35, 7, 34, 6, 33, 6, 32, 5, 31, 4, 30, 4, 29, 3, 28, 3, 27, 2, 26, 1, 25, 1, 24, 1, 23, 0, 22, 0, 21, 0, 20, 0, 19, 0, 18, 0, 17, 0, 16, 0, 15, 0, 14, 0, 13, 0, 12, 1, 11, 1, 10, 1, 9, 2, 8, 3, 7, 3, 6, 4, 5, 5, 4, 7, 3, 8, 2, 10, 1, 12, 0, 25, 0],
        type: "poly"
    };
    if (!n) {
        $("#locations-select").append('<option value="-1">-- Choose a location --</option>')
    }
    var m = e.length,
        g = null;
    for (var y = 0; y < m; y++) {
        g = e[y];
        $("#locations-select").append("<option value='" + y + "'>" + g[0] + "</option>")
    }
    $("#locations-select").change(function () {
        var e = $(this).val();
        b(e, true)
    });
    w();
    if (n) {
        b(0, true)
    }
    var E;
    S();
    var x = new google.maps.Marker({
        raiseOnDrag: false,
        icon: p,
        shadow: d,
        shape: v,
        map: E,
        position: E.getCenter()
    });
    x.info = new google.maps.InfoWindow({
        content: '<div class="contact-infobox">' + $("#contact address").html() + "</div>"
    });
    google.maps.event.addListener(x, "click", function () {
        x.info.open(E, x)
    });
    google.maps.event.addListener(x, "closeclick", function () {
        x.close()
    });

    

    // $("#contact-form button").click(function (e) {
    //     e.preventDefault();
    //     $("#contact-form #response").hide();
    //     $(this).parent().append('<img src="assets/images/loading.gif" class="loading" alt="Loading..." />');
    //     var t = $("#contact-form #inputName").val();
    //     var n = $("#contact-form #inputEmail").val();
    //     var o = $("#contact-form #textMessage").val();
    //     if (t == "") {
    //         N(i);
    //         return false
    //     } else if (!T(n)) {
    //         N(r);
    //         return false
    //     } else if (o == "") {
    //         N(s);
    //         return false
    //     }
    //     $.ajax({
    //         type: "post",
    //         url: "assets/libs/mail.php",
    //         data: "name=" + t + "&email=" + n + "&comments=" + o,
    //         success: function (e) {
    //             N(e, "success");
    //             $("#contact-form #inputName").val("");
    //             $("#contact-form #inputEmail").val("");
    //             $("#contact-form #textMessage").val("")
    //         },
    //         error: function (e) {
    //             N(e)
    //         }
    //     });
    //     return false
    // });
    $("#mailing-list button").click(function (e) {
        if (T($("#mailing-list #email").val())) {
            $("#mailing-list #response-newsletter").fadeOut();
            return true
        } else {
            e.preventDefault();
            $("#mailing-list #response-newsletter").hide().html('<span class="error">' + r + "</span>").fadeIn("slow");
            return false
        }
    });
    $("#switch .background a").click(function () {
        $("#theme").attr({
            href: "assets/stylesheets/themes/" + $(this).attr("id") + ".css"
        });
        $(".background .current").removeClass("current");
        $(this).toggleClass("current")
    });
    $("#switch .color a").click(function () {
        $("#color").attr({
            href: "assets/stylesheets/themes/" + $(this).attr("id") + ".css"
        });
        $(".color .current").removeClass("current");
        $(this).toggleClass("current");
        x.setIcon("assets/images/themes/" + $(this).attr("id") + "/marker.png");
        for (var e = 0; e < c.length; e++) {
            c[e].setIcon("assets/images/themes/" + $(this).attr("id") + "/marker.png")
        }
    });
    $("#show").css({
        "margin-left": "-200px"
    });
    $("#hide, #show").click(function () {
        if ($("#switch").is(":visible")) {
            $("#switch").animate({
                "margin-left": "-200px"
            }, 1e3, function () {
                $(this).hide()
            });
            $("#show").animate({
                "margin-left": "0px"
            }, 1e3).show()
        } else {
            $("#show").animate({
                "margin-left": "-200px"
            }, 1e3, function () {
                $(this).hide()
            });
            $("#switch").show().animate({
                "margin-left": "0"
            }, 1e3)
        }
    })
});