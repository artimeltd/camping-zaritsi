Camping Zaritsi Website
-----------------------

[ ![Codeship Status for artimeltd/Camping Zaritsi](https://codeship.com/projects/dd7cd880-820e-0132-b00c-129f3a2c461f/status?branch=master)](https://codeship.com/projects/57740)

Website for campingzaritsi.gr, built with Middleman ruby framework.

## Installation & build
```
$ bundle install
$ bundle exec middleman build
```

Copy `build/` directory to web server.